/**
@file
@brief This file handles all of the internal bits required to generate an AVI.

Copyright (C) 2016 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <stdlib.h>
#include <stdio.h>
#include "video.h"
#include "video_codec.h"

/* Constants: */

#define VID_RIFF 0x46464952 /**< @brief "RIFF" if read as a 4-byte string. */
#define VID_LIST 0x5453494C /**< @brief "LIST" if read as a 4-byte string. */
#define VID_AVIH 0x68697661 /**< @brief "avih" if read as a 4-byte string. */
#define VID_VIDS 0x73646976 /**< @brief "vids" if read as a 4-byte string. */
#define VID_STRH 0x68727473 /**< @brief "strh" if read as a 4-byte string. */
#define VID_STRF 0x66727473 /**< @brief "strf" if read as a 4-byte string. */
#define VID_00DB 0x62643030 /**< @brief "00db" if read as a 4-byte string. */
#define VID_00DC 0x63643030 /**< @brief "00dc" if read as a 4-byte string. */

/**
@brief This structure is used to store the current "state" of an AVI file.
*/

typedef struct {

  vid_size_t w;       /**< @brief The width of the video in pixels.       */
  vid_size_t h;       /**< @brief The height of the video in pixels.      */
  vid_rate_t fps;     /**< @brief The playback rate in frames-per-second. */
  vid_codec_t codec;  /**< @brief The video compression codec to be used. */

  int (*func)(vid_data_t*, void*);  /**< @brief Called once per video-frame. */
  void *data;                       /**< @brief A user-defined data pointer. */

  long offs_1;  /**< @brief The first "number of frames" offset in the file.  */
  long offs_2;  /**< @brief The second "number of frames" offset in the file. */

  vid_data_t *rgb;  /**< @brief A raw RGB data buffer, used to store a frame. */

} video_data_s;

/**
@brief Write a RIFF chunk to a file.

@param out The file to write data to.
@param chunk_type The 32-bit "chunk type" ID to use.
@param func A callback func, used to write out the actual chunk data.
@param data A user-defined pointer, passed to `func()` whenever it is called.
@return Zero on success, or non-zero on failure.
*/

static int vidPutRIFF(FILE *out, unsigned long chunk_type, int (*func)(FILE*, void*), void *data)
{
  long data_start, data_end;
  int result;

  if (!out) { return -1; }

  /* Output an RIFF header. This consists of a 4CC "type" and a 32-bit size: */
  vidPutUI32_LE(out, chunk_type);
  vidPutUI32_LE(out, 0);

  /* Output the actual chunk data: */
  data_start = ftell(out);
  if (func) { result = func(out, data); } else { result = 0; }
  data_end = ftell(out);

  /* Go back and update the "size" field in the RIFF chunk header: */
  fseek(out, data_start - 4, SEEK_SET);
  vidPutUI32_LE(out, (unsigned long)(data_end - data_start));
  fseek(out, data_end, SEEK_SET);

  /* Chunks must be aligned to 16-bit boundaries: */
  while(ftell(out) % 2) { fputc(0, out); }

  return result;
}

/**
@brief Output the main AVI header chunk for a .avi video file.

<http://msdn.microsoft.com/en-us/library/windows/desktop/dd318180(v=vs.85).aspx>

@param out The file to write data to.
@param data The current video "state" object.
@return Zero on success, or non-zero on failure.
*/

static int vidSaveAVI_avih(FILE *out, void *data)
{
  video_data_s *video = (video_data_s*)data;
  if (!video) { return -1; }

  vidPutUI32_LE(out, 1000000 / video->fps); /* DWORD dwMicroSecPerFrame;    */
  vidPutUI32_LE(out, video->w * video->h * 3 * video->fps);  /* DWORD dwMaxBytesPerSec; */
  vidPutUI32_LE(out, 4);                    /* DWORD dwPaddingGranularity;  */
  vidPutUI32_LE(out, 0);                    /* DWORD dwFlags;               */
  video->offs_1 = ftell(out); vidPutUI32_LE(out, 0);        /* DWORD dwTotalFrames;         */
  vidPutUI32_LE(out, 0);                    /* DWORD dwInitialFrames;       */
  vidPutUI32_LE(out, 1);                    /* DWORD dwStreams;             */
  vidPutUI32_LE(out, 0);                    /* DWORD dwSuggestedBufferSize; */
  vidPutUI32_LE(out, video->w);             /* DWORD dwWidth;               */
  vidPutUI32_LE(out, video->h);             /* DWORD dwHeight;              */
  vidPutUI32_LE(out, 0);                    /* DWORD dwReserved[0];         */
  vidPutUI32_LE(out, 0);                    /* DWORD dwReserved[1];         */
  vidPutUI32_LE(out, 0);                    /* DWORD dwReserved[2];         */
  vidPutUI32_LE(out, 0);                    /* DWORD dwReserved[3];         */

  return 0;
}

/**
@brief Output a single stream header chunk for a .avi video file.

<http://msdn.microsoft.com/en-us/library/windows/desktop/dd318183(v=vs.85).aspx>

@param out The file to write data to.
@param data The current video "state" object.
@return Zero on success, or non-zero on failure.
*/

static int vidSaveAVI_strh(FILE *out, void *data)
{
  video_data_s *video = (video_data_s*)data;
  if (!video) { return -1; }

  vidPutUI32_LE(out, VID_VIDS);           /* FOURCC fccType;               */
  vidPutUI32_LE(out, video->codec);       /* FOURCC fccHandler;            */
  vidPutUI32_LE(out, 0);                  /* DWORD  dwFlags;               */
  vidPutUI16_LE(out, 0);                  /* WORD   wPriority;             */
  vidPutUI16_LE(out, 'e' + ('n' << 8));   /* WORD   wLanguage;             */
  vidPutUI32_LE(out, 0);                  /* DWORD  dwInitialFrames;       */
  vidPutUI32_LE(out, 1);                  /* DWORD  dwScale;               */
  vidPutUI32_LE(out, video->fps);         /* DWORD  dwRate;                */
  vidPutUI32_LE(out, 0);                  /* DWORD  dwStart;               */
  video->offs_2 = ftell(out); vidPutUI32_LE(out, 0);      /* DWORD  dwLength;              */
  vidPutUI32_LE(out, 0);                  /* DWORD  dwSuggestedBufferSize; */
  vidPutUI32_LE(out, 10000);              /* DWORD  dwQuality;             */
  vidPutUI32_LE(out, 0);                  /* DWORD  dwSampleSize;          */
  vidPutUI16_LE(out, 0);                  /* short int left;               */
  vidPutUI16_LE(out, 0);                  /* short int top;                */
  vidPutUI16_LE(out, video->w);           /* short int right;              */
  vidPutUI16_LE(out, video->h);           /* short int bottom;             */

  return 0;
}

/**
@brief Output a single stream format chunk for a .avi video file.

<http://msdn.microsoft.com/en-us/library/windows/desktop/dd318229(v=vs.85).aspx>

@param out The file to write data to.
@param data The current video "state" object.
@return Zero on success, or non-zero on failure.
*/

static int vidSaveAVI_strf(FILE *out, void *data)
{
  const video_data_s *video = (video_data_s*)data;
  if (!video) { return -1; }

  vidPutUI32_LE(out, 40);           /* DWORD biSize;          */
  vidPutUI32_LE(out, video->w);     /* LONG  biWidth;         */
  vidPutUI32_LE(out, video->h);     /* LONG  biHeight;        */
  vidPutUI16_LE(out, 1);            /* WORD  biPlanes;        */
  vidPutUI16_LE(out, 24);           /* WORD  biBitCount;      */
  vidPutUI32_LE(out, video->codec); /* DWORD biCompression;   */
  vidPutUI32_LE(out, 0);            /* DWORD biSizeImage;     */
  vidPutUI32_LE(out, 2048);         /* LONG  biXPelsPerMeter; */
  vidPutUI32_LE(out, 2048);         /* LONG  biYPelsPerMeter; */
  vidPutUI32_LE(out, 0);            /* DWORD biClrUsed;       */
  vidPutUI32_LE(out, 0);            /* DWORD biClrImportant;  */

  return 0;
}

/**
@brief Output the list-of-stream-headers chunk for a .avi video file.

@param out The file to write data to.
@param data The current video "state" object.
@return Zero on success, or non-zero on failure.
*/

static int vidSaveAVI_strl(FILE *out, void *data)
{
  fprintf(out, "strl");
  vidPutRIFF(out, VID_STRH, vidSaveAVI_strh, data);
  vidPutRIFF(out, VID_STRF, vidSaveAVI_strf, data);
  return 0;
}

/**
@brief Output the main header chunk for a .avi video file.

This chunk contains the main "avih" file header, along with the headers for all
individual streams within the file.

@param out The file to write data to.
@param data The current video "state" object.
@return Zero on success, or non-zero on failure.
*/

static int vidSaveAVI_hdrl(FILE *out, void *data)
{
  fprintf(out, "hdrl");
  vidPutRIFF(out, VID_AVIH, vidSaveAVI_avih, data);
  vidPutRIFF(out, VID_LIST, vidSaveAVI_strl, data);
  return 0;
}

/**
@brief Output a single frame of video data.

@param out The file to write data to.
@param data The current video "state" object.
@return Zero on success, or non-zero on failure.
*/

static int vidSaveAVI_frame(FILE *out, void *data)
{
  video_data_s *video = (video_data_s*)data;
  int keep_going = video->func(video->rgb, video->data);

  switch (video->codec)
  {
    case VID_CODEC_NONE:
      vidSaveAVI_NONE(out, video->w, video->h, video->rgb);
    break;
    case VID_CODEC_8BPS:
      vidSaveAVI_8BPS(out, video->w, video->h, video->rgb);
    break;
    case VID_CODEC_AYUV:
      vidSaveAVI_AYUV(out, video->w, video->h, video->rgb);
    break;
    case VID_CODEC_R210:
      vidSaveAVI_R210(out, video->w, video->h, video->rgb);
    break;
    case VID_CODEC_Y800: case VID_CODEC_GREY: case VID_CODEC_Y8:
      vidSaveAVI_Y800(out, video->w, video->h, video->rgb);
    break;
    default: return 0xBADC0DEC;
  }

  /* If the user returns a "false" value, it's time to end the video: */
  return keep_going ? 0 : -1;
}

/**
@brief Output the list-of-frames chunk within a single stream.

@param out The file to write data to.
@param data The current video "state" object.
@return Zero on success, or non-zero on failure.
*/

static int vidSaveAVI_rec(FILE *out, void *data)
{
  const video_data_s *video = (video_data_s*)data;
  unsigned long num_frames, frame_type;
  long current_offset;

  if (!video) { return -1; }

  fprintf(out, "rec ");

  /* Continue outputting frames until "false" is returned: */
  frame_type = video->codec == VID_CODEC_NONE ? VID_00DB : VID_00DC;
  num_frames = 1;
  while (vidPutRIFF(out, frame_type, vidSaveAVI_frame, data) == 0) { ++num_frames; }

  /* Update the "dummy" frame counts specified earlier with the real value: */
  current_offset = ftell(out);
  fseek(out, video->offs_1, SEEK_SET); vidPutUI32_LE(out, num_frames);
  fseek(out, video->offs_2, SEEK_SET); vidPutUI32_LE(out, num_frames);
  fseek(out, current_offset, SEEK_SET);

  return 0;
}

/**
@brief Output the list-of-data-streams chunk for a .avi video file.

@param out The file to write data to.
@param data The current video "state" object.
@return Zero on success, or non-zero on failure.
*/

static int vidSaveAVI_movi(FILE *out, void *data)
{
  fprintf(out, "movi");
  return vidPutRIFF(out, VID_LIST, vidSaveAVI_rec, data);
}

/**
@brief Output the main "AVI " chunk for a .avi video file.

<http://msdn.microsoft.com/en-us/library/windows/desktop/dd318189(v=vs.85).aspx>

@param out The file to write data to.
@param data The current video "state" object.
@return Zero on success, or non-zero on failure.
*/

static int vidSaveAVI_avi(FILE *out, void *data)
{
  fprintf(out, "AVI ");
  vidPutRIFF(out, VID_LIST, vidSaveAVI_hdrl, data);
  vidPutRIFF(out, VID_LIST, vidSaveAVI_movi, data);
  return 0;
}

/*
[PUBLIC] Open an AVI file and write video data to it until told to stop.
*/

int vidSaveAVI(
    const char *file_name,
    vid_size_t w,
    vid_size_t h,
    vid_rate_t fps,
    vid_codec_t codec,
    int (*func)(vid_data_t*, void*),
    void *data)
{
  FILE *out;
  video_data_s video;
  int status;

  /* Make sure that what the user has asked for is reasonable: */
  if (!file_name || w < 1 || h < 1 || fps < 1 || !func) { return -1; }

  /* Initialise an internal "state tracker" structure: */
  video.w = w;
  video.h = h;
  video.fps = fps;
  video.codec = codec;
  video.data = data;
  video.func = func;

  /* Allocate just enough memory to store a single, uncompressed video frame: */
  video.rgb = (vid_data_t*)malloc(w * h * sizeof(vid_data_t) * 3);
  if (!video.rgb) { return -2; }

  /* Attempt to open an output file: */
  out = fopen(file_name, "wb");
  if (!out)
  {
    free(video.rgb);
    return -3;
  }

  /* Start writing video data to the output file: */
  status = vidPutRIFF(out, VID_RIFF, vidSaveAVI_avi, &video);

  /* Clean everything up: */
  free(video.rgb);
  fclose(out);
  return status;
}
