/**
@file
@brief This file exposes the codec functions used to encode the video data.

Copyright (C) 2016 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef __QQQ_VIDEO_CODEC_H__
#define __QQQ_VIDEO_CODEC_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include "video.h"

/**
@brief Write a little-endian UINT16 value to a file.

@param out The file to write to.
@param value The value to write.
*/

void vidPutUI16_LE(FILE *out, unsigned short value);

/**
@brief Write a big-endian UINT16 value to a file.

@param out The file to write to.
@param value The value to write.
*/

void vidPutUI16_BE(FILE *out, unsigned short value);

/**
@brief Write a little-endian UINT32 value to a file.

@param out The file to write to.
@param value The value to write.
*/

void vidPutUI32_LE(FILE *out, unsigned long value);

/**
@brief Write a big-endian UINT32 value to a file.

@param out The file to write to.
@param value The value to write.
*/

void vidPutUI32_BE(FILE *out, unsigned long value);

/**
@brief Output a single video frame as raw BGR data.

This is identical to the pixel format of a standard 24-bit Windows BMP file.

@param out The file to write data to.
@param w The width of the RGB data in pixels.
@param h The height of the RGB data in pixels.
@param data The actual pixel RGB data.
@return Zero on success, or non-zero on failure.
*/

int vidSaveAVI_NONE(FILE *out, vid_size_t w, vid_size_t h, vid_data_t *data);

/**
@brief Output a single video frame using Quicktime planar RLE compression.

<http://web.archive.org/web/20040805075908/http://home.pcisys.net/~melanson/codecs/8bps.txt>

@param out The file to write data to.
@param w The width of the RGB data in pixels.
@param h The height of the RGB data in pixels.
@param data The actual pixel RGB data.
@return Zero on success, or non-zero on failure.
*/

int vidSaveAVI_8BPS(FILE *out, vid_size_t w, vid_size_t h, vid_data_t *data);

/**
@brief Output a single video frame as raw 10-bit RGB data.

See <https://wiki.multimedia.cx/index.php?title=R210>

At time of writing, SMPlayer supports this codec, but VLC does not.

@param out The file to write data to.
@param w The width of the RGB data in pixels.
@param h The height of the RGB data in pixels.
@param data The actual pixel RGB data.
@return Zero on success, or non-zero on failure.
*/

int vidSaveAVI_R210(FILE *out, vid_size_t w, vid_size_t h, vid_data_t *data);

/**
@brief Output a single video frame in uncompressed alpha + YUV format.

See <http://www.fourcc.org/yuv.php#AYUV>

At time of writing, SMPlayer supports this codec, but VLC does not.

@param out The file to write data to.
@param w The width of the RGB data in pixels.
@param h The height of the RGB data in pixels.
@param data The actual pixel RGB data.
@return Zero on success, or non-zero on failure.
*/

int vidSaveAVI_AYUV(FILE *out, vid_size_t w, vid_size_t h, vid_data_t *data);

/**
@brief Output a single video frame in uncompressed luminance-only format.

See <http://www.fourcc.org/yuv.php#Y800>

@param out The file to write data to.
@param w The width of the RGB data in pixels.
@param h The height of the RGB data in pixels.
@param data The actual pixel RGB data.
@return Zero on success, or non-zero on failure.
*/

int vidSaveAVI_Y800(FILE *out, vid_size_t w, vid_size_t h, vid_data_t *data);

#ifdef __cplusplus
}
#endif

#endif /* __QQQ_VIDEO_CODEC_H__ */
