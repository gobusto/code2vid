/**
@file
@brief This file contains the codec functions used to encode the video data.

Copyright (C) 2016 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <stdlib.h>
#include "video_codec.h"

/**
@brief Convert an RGB value into YCbCr format.

See <http://www.fourcc.org/fccyvrgb.php>

@param rgb Pointer to the "input" triad of R/G/B values.
@param ycbcr Pointer to the "output" triad of Y/Cb/Cr values.
*/

static void vidPixelAsYCbCr(const unsigned char *rgb, unsigned char *ycbcr)
{
  double r, g, b;
  double y, cb, cr;

  r = rgb[0] / 255.0;
  g = rgb[1] / 255.0;
  b = rgb[2] / 255.0;

  y  =  (0.299 * r) + (0.587 * g) + (0.114 * b);
  cb = -(0.169 * r) - (0.331 * g) + (0.500 * b);
  cr =  (0.500 * r) - (0.419 * g) - (0.081 * b);

  cb += 0.5;
  cr += 0.5;

  ycbcr[0] = 16 + (unsigned char)(y  * 219);
  ycbcr[1] = 16 + (unsigned char)(cb * 223);
  ycbcr[2] = 16 + (unsigned char)(cr * 223);

  if      (y  < 0) { ycbcr[0] = 16;  }
  else if (y  > 1) { ycbcr[0] = 235; }
  if      (cb < 0) { ycbcr[1] = 16;  }
  else if (cb > 1) { ycbcr[1] = 239; }
  if      (cr < 0) { ycbcr[2] = 16;  }
  else if (cr > 1) { ycbcr[2] = 239; }
}

/*
[PUBLIC] Write a little-endian UINT16 value to a file.
*/

void vidPutUI16_LE(FILE *out, unsigned short value)
{
  fputc((value >> 0) & 0xFF, out);
  fputc((value >> 8) & 0xFF, out);
}

/*
[PUBLIC] Write a big-endian UINT16 value to a file.
*/

void vidPutUI16_BE(FILE *out, unsigned short value)
{
  fputc((value >> 8) & 0xFF, out);
  fputc((value >> 0) & 0xFF, out);
}

/*
[PUBLIC] Write a little-endian UINT32 value to a file.
*/

void vidPutUI32_LE(FILE *out, unsigned long value)
{
  fputc((value >> 0 ) & 0xFF, out);
  fputc((value >> 8 ) & 0xFF, out);
  fputc((value >> 16) & 0xFF, out);
  fputc((value >> 24) & 0xFF, out);
}

/*
[PUBLIC] Write a big-endian UINT32 value to a file.
*/

void vidPutUI32_BE(FILE *out, unsigned long value)
{
  fputc((value >> 24) & 0xFF, out);
  fputc((value >> 16) & 0xFF, out);
  fputc((value >> 8 ) & 0xFF, out);
  fputc((value >> 0 ) & 0xFF, out);
}

/*
[PUBLIC] Output a single video frame as raw BGR data.
*/

int vidSaveAVI_NONE(FILE *out, vid_size_t w, vid_size_t h, vid_data_t *data)
{
  vid_size_t x, y;

  /* NOTE: The video data is stored upside-down, in BGR format: */
  for (y = h; y > 0; --y)
  {
    for (x = 0; x < w; ++x)
    {
      unsigned long i = vidPixelOffset(w, x, y-1);
      fputc(data[i + 2], out);
      fputc(data[i + 1], out);
      fputc(data[i + 0], out);
    }
    /* Align each scanline to 4 bytes: */
    for (x *= 3; x % 4; ++x) { fputc(0, out); }
  }

  return 0;
}
/*
[PUBLIC] Output a single video frame using Quicktime planar RLE compression.
*/

int vidSaveAVI_8BPS(FILE *out, vid_size_t w, vid_size_t h, vid_data_t *data)
{
  const long start_of_frame_chunk = ftell(out);
  vid_size_t y, p;

  /* First comes the compressed length of each line, for each colour plane: */
  for (p = 0; p < 3; ++p)
  {
    for (y = 0; y < h; ++y) { vidPutUI16_BE(out, 0); }
  }

  /* Next comes the pixel data for each plane - first R, then G, then B: */
  for (p = 0; p < 3; ++p)
  {
    for (y = 0; y < h; ++y)
    {
      long row_start_offset = ftell(out);
      long compressed_row_length;
      vid_size_t x;

      /* Output RLE-compressed data for the current pixel/plane: */
      for (x = 0; x < w;)
      {
        unsigned long offs = vidPixelOffset(w, x, y) + p;
        unsigned char same, diff;

        /* Try to find a run of up to 128 different-colour pixels: */
        for (diff = 1; diff < 128 && x + diff < w; ++diff)
        {
          if (data[offs] == data[vidPixelOffset(w, x + diff, y) + p]) { break; }
        }

        /* Try to find a run of up to 129 same-colour pixels: */
        for (same = 1; same < 129 && x + same < w; ++same)
        {
          if (data[offs] != data[vidPixelOffset(w, x + same, y) + p]) { break; }
        }

        if (diff > same)
        {
          unsigned char i;
          /* Values of 0-127 indicate that the next `diff` bytes can be read *
           * as standard pixel values with no RLE compression bytes present. */
          fputc(diff - 1, out);
          for (i = 0; i < diff; ++i)
          {
            fputc(data[vidPixelOffset(w, x + i, y) + p], out);
          }
          x += diff;
        }
        else
        {
          /* Note that `same` will always be at least 2, so `257 - 2 = 255`. */
          fputc(257 - same, out);
          fputc(data[offs], out);
          x += same;
        }
      }

      compressed_row_length = ftell(out) - row_start_offset;

      /* FIXME: This should be a fatal error! */
      if (compressed_row_length > 65535)
      {
        fprintf(stderr, "[8BPS] Error: Compressed scanline is too long!\n");
      }

      /* Replace the "dummy" row-length value with the actual value: */
      fseek(out, start_of_frame_chunk + (p * h * 2) + (y * 2), SEEK_SET);
      vidPutUI16_BE(out, (unsigned short)compressed_row_length);
      fseek(out, row_start_offset + compressed_row_length, SEEK_SET);
    }
  }

  return 0;
}

/*
[PUBLIC] Output a single video frame as raw 10-bit RGB data.
*/

int vidSaveAVI_R210(FILE *out, vid_size_t w, vid_size_t h, vid_data_t *data)
{
  vid_size_t x, y;

  for (y = 0; y < h; ++y)
  {
    for (x = 0; x < w; ++x)
    {
      unsigned long i = vidPixelOffset(w, x, y);
      unsigned long value = 0;

      value += ((unsigned short)data[i+0] << 2) << 20;
      value += ((unsigned short)data[i+1] << 2) << 10;
      value += ((unsigned short)data[i+2] << 2);

      vidPutUI32_BE(out, value);
    }
    /* Align each scanline to 256 bytes: */
    for (x *= 4; x % 256; ++x) { fputc(0, out); }
  }

  return 0;
}

/*
[PUBLIC] Output a single video frame in uncompressed alpha + YUV format.
*/

int vidSaveAVI_AYUV(FILE *out, vid_size_t w, vid_size_t h, vid_data_t *data)
{
  vid_size_t x, y;
  unsigned char ycbcr[3];

  for (y = 0; y < h; ++y)
  {
    for (x = 0; x < w; ++x)
    {
      vidPixelAsYCbCr(&data[vidPixelOffset(w, x, y)], ycbcr);

      fputc(ycbcr[2], out);
      fputc(ycbcr[1], out);
      fputc(ycbcr[0], out);
      fputc(255, out);
    }
  }

  return 0;
}

/*
[PUBLIC] Output a single video frame in uncompressed luminance-only format.
*/

int vidSaveAVI_Y800(FILE *out, vid_size_t w, vid_size_t h, vid_data_t *data)
{
  vid_size_t x, y;
  unsigned char ycbcr[3];

  for (y = 0; y < h; ++y)
  {
    for (x = 0; x < w; ++x)
    {
      vidPixelAsYCbCr(&data[vidPixelOffset(w, x, y)], ycbcr);
      fputc(ycbcr[0], out);
    }
  }

  return 0;
}
