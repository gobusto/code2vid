/**
@file
@brief This file exposes all public aspects of the AVI-producing code.

Copyright (C) 2016 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef __QQQ_VIDEO_H__
#define __QQQ_VIDEO_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Common data types: */

typedef unsigned long vid_size_t; /**< @brief Size of width/height values. */
typedef unsigned long vid_rate_t; /**< @brief Size of any FPS rate values. */
typedef unsigned char vid_data_t; /**< @brief Size of pixels R/G/B values. */

/**
@brief Enumerates the available video codecs for video output:
*/

typedef enum {

  /* Standard 4CCs: */
  VID_CODEC_NONE = 0x00000000,  /**< @brief Uncompressed, BGR-format images. */
  VID_CODEC_8BPS = 0x53504238,  /**< @brief Quicktime planar RLE compressed. */
  VID_CODEC_AYUV = 0x56555941,  /**< @brief Uncompressed alpha + YUV values. */
  VID_CODEC_R210 = 0x30313272,  /**< @brief Uncompressed, 10-bit RGB values. */
  VID_CODEC_Y800 = 0x30303859,  /**< @brief Uncompressed Y-component values. */

  /* Alternative 4CCs for some of the codecs listed above: */
  VID_CODEC_GREY = 0x59455247,  /**< @brief Works as per `VID_CODEC_Y800`. */
  VID_CODEC_Y8   = 0x20203859   /**< @brief Works as per `VID_CODEC_Y800`. */

} vid_codec_t;

/**
@brief Calculate the offset to a particular pixel within a 24-bit image.

@param w The width of the image in pixels.
@param x The X position of the pixel.
@param y The Y position of the pixel.
@return The offset of the first byte of that pixel RGB value.
*/

#define vidPixelOffset(w,x,y) (((y)*(w)*3) + ((x)*3))

/**
@brief Open an AVI file and write video data to it until told to stop.

@todo This should probably return a status code, rather than a simple `int`...

@param file_name The name of (and path to) the AVI file to create/replace.
@param w The width of the video in pixels.
@param h The height of the video in pixels.
@param fps The playback rate of the video in frames-per-second.
@param codec The video codec to use when compressing the video data.
@param func A callback function, called once per frame to provide image data.
@param data An optional, user-defined pointer to pass to the callback function.
@return Returns zero on success, or non-zero if something went wrong.
*/

int vidSaveAVI(
    const char *file_name,
    vid_size_t w,
    vid_size_t h,
    vid_rate_t fps,
    vid_codec_t codec,
    int (*func)(vid_data_t*, void*),
    void *data);

#ifdef __cplusplus
}
#endif

#endif /* __QQQ_VIDEO_H__ */
