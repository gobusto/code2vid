/**
@file
@brief A fairly simple example of how an AVI can be generated purely from code.

This file is for demonstration purposes; it is not part of the actual AVI code.

Copyright (C) 2016 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "video/video.h"

/**
@brief This structure is used to store the current "state" between AVI frames.
*/

typedef struct {

  /* IMPORTANT, DO-NOT-CHANGE-ON-THE-FLY THINGS: */
  vid_size_t w; /**< @brief The width of the video in pixels.  */
  vid_size_t h; /**< @brief The height of the video in pixels. */

  /* CURRENT-STATE VARIABLES: */
  int        frame;   /**< @brief The number of frames drawn thus far. */
  vid_data_t rgb[3];  /**< @brief The current drawing colour.          */

  /* OTHER STUFF: */
  vid_data_t *out;  /**< @brief Points to the actual video "output" buffer. */

} screen_s;

/**
@brief Change the current drawing colour.

@param screen The "state" structure to update.
@param r The new "red" value.
@param g The new "green" value.
@param b The new "blue" value.
*/

static void gfxSetRGB(screen_s *screen, vid_data_t r, vid_data_t g, vid_data_t b)
{
  if (!screen) { return; }

  screen->rgb[0] = r;
  screen->rgb[1] = g;
  screen->rgb[2] = b;
}

/**
@brief Fill the current video frame with the current drawing colour.

@param screen The "state" structure to update.
*/

static void gfxClear(screen_s *screen)
{
  unsigned long i;

  if (!screen) { return; }

  for (i = 0; i < screen->w * screen->h * 3; ++i) { screen->out[i] = screen->rgb[i % 3]; }
}

/**
@brief Change the colour of a single pixel to match the current drawing colour.

@param screen The "state" structure to update.
@param x The X position of the pixel to change.
@param y The Y position of the pixel to change.
*/

static void gfxDrawPoint(screen_s *screen, vid_size_t x, vid_size_t y)
{
  unsigned long i;

  if (!screen || x >= screen->w || y >= screen->h) { return; }

  i = vidPixelOffset(screen->w, x, y);

  screen->out[i+0] = screen->rgb[0];
  screen->out[i+1] = screen->rgb[1];
  screen->out[i+2] = screen->rgb[2];
}

/**
@brief Draw a straight line from one point to another.

@param screen The "state" structure to update.
@param x1 The X position of the first point.
@param y1 The Y position of the first point.
@param x2 The X position of the second point
@param y2 The Y position of the second point.
*/

static void gfxDrawLine(screen_s *screen, vid_size_t x1, vid_size_t y1, vid_size_t x2, vid_size_t y2)
{
  vid_size_t dx, dy, steps, i;

  if (!screen || x1 >= screen->w || y1 >= screen->h || x2 >= screen->w || y2 >= screen->h) { return; }

  dx = (x1 > x2) ? (x1 - x2) : (x2 - x1);
  dy = (y1 > y2) ? (y1 - y2) : (y2 - y1);
  steps = (dx > dy) ? dx : dy;

  for (i = 0; i <= steps; ++i)
  {
    vid_size_t px, py;

    if (dx > dy)
    {
      px = (x1 > x2) ? (x1 -   i               ) : (x1 +   i               );
      py = (y1 > y2) ? (y1 - ((i * dy) / steps)) : (y1 + ((i * dy) / steps));
    }
    else
    {
      py = (y1 > y2) ? (y1 -   i               ) : (y1 +   i               );
      px = (x1 > x2) ? (x1 - ((i * dx) / steps)) : (x1 + ((i * dx) / steps));
    }

    gfxDrawPoint(screen, px, py);
  }

}

/**
@brief This is a callback function, called once for each frame of video.

@param video_rgb_buffer A raw RGB data buffer for the current video frame.
@param user_defined_data This is whatever the user passed to `vidSaveAVI()`.
@return Returns `false` once the final frame of video data has been drawn.
*/

static int gfxDraw(vid_data_t *video_rgb_buffer, void *user_defined_data)
{
  screen_s *screen = (screen_s*)user_defined_data;
  vid_size_t x;

  /* Copy the output buffer address so we don't need to pass it to functions: */
  screen->out = video_rgb_buffer;

  /* Clear the display: */
  gfxSetRGB(screen, 0, 0, 32);
  gfxClear(screen);

  /* Draw something: */
  for (x = 1; x < screen->w; ++x)
  {
    const double a1 = ((x-1) + screen->frame) * 0.05;
    const double a2 = ((x-0) + screen->frame) * 0.05;
    vid_size_t sy1 = (vid_size_t)(sin(a1) * (screen->h / 4)) + (screen->h / 2);
    vid_size_t sy2 = (vid_size_t)(sin(a2) * (screen->h / 4)) + (screen->h / 2);
    vid_size_t cy1 = (vid_size_t)(cos(a1) * (screen->h / 4)) + (screen->h / 2);
    vid_size_t cy2 = (vid_size_t)(cos(a2) * (screen->h / 4)) + (screen->h / 2);
    gfxSetRGB(screen, 0, 255, 0); gfxDrawLine(screen, x-1, cy1, x, cy2);
    gfxSetRGB(screen, 255, 0, 0); gfxDrawLine(screen, x-1, sy1, x, sy2);
  }

  /* Increase the frame counter so that we know where we're up to: */
  ++screen->frame;
  return screen->frame < 300;
}

/**
@brief The program entry point.

@return Zero on success, or non-zero on failure.
*/

int main(void)
{
  screen_s screen;

  /* Set up some basic "screen" properties: */
  screen.w = 320;
  screen.h = 200;
  screen.frame = 0;

  /* Attempt to create an AVI file: */
  if (vidSaveAVI("out.avi", screen.w, screen.h, 60, VID_CODEC_8BPS, gfxDraw, &screen) == 0)
    printf("Video saved successfully!\n");
  else
    printf("Something went wrong...\n");
  return 0;
}
