code2vid
========

This project provides a very simple way to output AVI-format video files from C
programs. It has two main goals:

+ **Simplicity.** The code should be as easy-to-understand as possible.
+ **Independence.** No other libraries are required to compile or use the code.

Note that there is no support for audio streams; only video data is output.

How to use
----------

Simply add the files in `video/` to your project, and call `vidSaveAVI()`:

    #include <stdlib.h>
    #include "video/video.h"

    /* This function is used to draw each frame: */
    static int draw(vid_data_t *image, void *user_data)
    {
      static int number_of_frames = 0;
      long i;

      /* Fill the current frame with red pixels: */
      for (i = 0; i < 1280 * 720; ++i)
      {
        image[(i * 3) + 0] = 255; /* Red   */
        image[(i * 3) + 1] = 0;   /* Green */
        image[(i * 3) + 2] = 0;   /* Blue  */
      }

      /* End the video after 100 frames have been drawn: */
      ++number_of_frames;
      return number_of_frames < 100;
    }

    /* Output an uncompressed 720p video with a playback rate of 25 FPS: */
    int main(void)
    {
      return vidSaveAVI("out.avi", 1280, 720, 25, VID_CODEC_NONE, draw, NULL);
    }

Each frame of video data is represented as a simple list of RGB pixels, each of
which is stored as a triad of `unsigned char` values: `RGBRGBRGBRGB...`

For a more complex example, see the `main.c` file of the Code::Blocks "example"
project included with this code.

What codecs are supported?
--------------------------

The following is a complete list of currently-supported video codecs:

+ Uncompressed 24-bit BGR.
+ Quicktime "8BPS" planar RLE-compression.
+ Uncompressed "AYUV" YCbCr.
+ Uncompressed "r210" 10-bits-per-channel RGB.
+ Uncompressed "Y800" greyscale.

Support for additional codecs may be added in the future.

Of course, it's always possible to convert the videos into another format - for
example, using [FFMPEG](https://ffmpeg.org): `ffmpeg -i input.avi output.mp4`

Licensing
---------

This code is made available under the [MIT](http://opensource.org/licenses/MIT)
license, which allows you to use or modify it for any purpose including paid-for
and closed-source projects. All I ask in return is that proper attribution is
given (i.e. don't remove my name from the copyright text in the source code, and
perhaps include me on the "credits" screen, if your program has such a thing).
